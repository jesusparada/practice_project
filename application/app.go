package application

import (
	"fmt"
	"project/domain"
)

//go:generate mockgen -destination=./mocks/data_mock.go -package=mocks -source=./app.go

var (
	ErrorFindingActor = fmt.Errorf("error finding actor")
	ErrorFindingFilm  = fmt.Errorf("error finding film")
)

type Data interface {
	GetActorInfo(actorID *int) (*domain.ActorInfo, error)
	GetFilmInfo(filmID string) (*domain.FilmInfo, error)
}

type app struct {
	data Data
}

func NewApp(data Data) *app {
	return &app{data: data}
}

func (a *app) GetFilmsByActor(actorID *int) ([]string, error) {
	actorInfo, err := a.data.GetActorInfo(actorID)
	if err != nil {
		return nil, ErrorFindingActor
	}
	filmsByActor := make([]string, 0)
	for _, f := range actorInfo.Films {
		film, err := a.data.GetFilmInfo(f)
		if err != nil {
			return nil, ErrorFindingFilm
		}
		filmsByActor = append(filmsByActor, film.Title)
	}

	return filmsByActor, nil
}

func (a *app) GetFilmsByActorWithRoutines(actorID *int) ([]string, error) {
	actorInfo, err := a.data.GetActorInfo(actorID)
	if err != nil {
		return nil, err
	}
	chanResponse := make(chan *domain.FilmInfo, len(actorInfo.Films))
	chanError := make(chan error)

	for _, f := range actorInfo.Films {
		go func(url string, responseChannel chan *domain.FilmInfo, errCh chan error) {
			film, err := a.data.GetFilmInfo(url)
			if err != nil {
				errCh <- err
				return
			}
			responseChannel <- film
		}(f, chanResponse, chanError)
	}

	counter := 0
	filmsByActor := make([]string, 0)
	for {
		select {
		case film := <-chanResponse:
			filmsByActor = append(filmsByActor, film.Title)
			counter++
		case err := <-chanError:
			return nil, err
		}
		if counter == len(actorInfo.Films) {
			break
		}
	}

	return filmsByActor, nil
}

package application

import (
	"errors"
	"github.com/golang/mock/gomock"
	"project/application/mocks"
	"project/domain"
	"reflect"
	"testing"
)

func Test_app_GetFilmsByActor(t *testing.T) {

	var (
		actorID = 1
		actor   = domain.ActorInfo{
			Name:   "Luke Skywalker",
			Height: "172",
			Films: []string{
				"https://swapi.dev/api/films/1/",
				"https://swapi.dev/api/films/2/",
				"https://swapi.dev/api/films/3/"},
		}
		filmsTitles  = []string{"Title 1", "Title 2", "Title 3"}
		film1        = domain.FilmInfo{Title: filmsTitles[0]}
		film2        = domain.FilmInfo{Title: filmsTitles[1]}
		film3        = domain.FilmInfo{Title: filmsTitles[2]}
		genericError = errors.New("not found")
	)
	type fields struct {
		data func(m *mocks.MockData)
	}
	type args struct {
		actorID *int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
		err     error
	}{
		{
			name: "SUCCESS: Get Films Name successful",
			fields: fields{
				data: func(m *mocks.MockData) {
					m.EXPECT().GetActorInfo(&actorID).
						Return(&actor, nil)
					m.EXPECT().GetFilmInfo(actor.Films[0]).
						Return(&film1, nil)
					m.EXPECT().GetFilmInfo(actor.Films[1]).
						Return(&film2, nil)
					m.EXPECT().GetFilmInfo(actor.Films[2]).
						Return(&film3, nil)
				},
			},
			args:    args{actorID: &actorID},
			want:    filmsTitles,
			wantErr: false,
			err:     nil,
		},
		{
			name: "ERROR: Actor not found",
			fields: fields{
				data: func(m *mocks.MockData) {
					m.EXPECT().GetActorInfo(&actorID).
						Return(nil, genericError)
				},
			},
			args:    args{actorID: &actorID},
			want:    nil,
			wantErr: true,
			err:     ErrorFindingActor,
		},
		{
			name: "ERROR: One film not found",
			fields: fields{
				data: func(m *mocks.MockData) {
					m.EXPECT().GetActorInfo(&actorID).
						Return(&actor, nil)
					m.EXPECT().GetFilmInfo(actor.Films[0]).
						Return(&film1, nil)
					m.EXPECT().GetFilmInfo(actor.Films[1]).
						Return(&film2, nil)
					m.EXPECT().GetFilmInfo(actor.Films[2]).
						Return(nil, genericError)
				},
			},
			args:    args{actorID: &actorID},
			want:    nil,
			wantErr: true,
			err:     ErrorFindingFilm,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mockData := mocks.NewMockData(mockCtrl)
			if tt.fields.data != nil {
				tt.fields.data(mockData)
			}
			a := &app{
				data: mockData,
			}
			got, err := a.GetFilmsByActor(tt.args.actorID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFilmsByActor() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(err, tt.err) {
				t.Errorf("GetFilmsByActor() err = %v, wantErr %v", got, tt.want)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFilmsByActor() got = %v, want %v", got, tt.want)
			}

		})
	}
}

package domain

type ActorInfo struct {
	Name   string
	Height string
	Films  []string
}

type FilmInfo struct {
	Title string
}

package infra

import (
	"encoding/json"
	"fmt"
	"log"
	"project/domain"
	"project/infra/utils"
)

type externalApi struct {
}

func NewExternalAPI() *externalApi {
	return &externalApi{}
}

func (e externalApi) GetActorInfo(actorID *int) (*domain.ActorInfo, error) {
	log.Println("Searching actor with id", *actorID)
	url := fmt.Sprintf("https://swapi.dev/api/people/%d", *actorID)
	responseBytes, err := utils.Get(url)
	if err != nil {
		return nil, err
	}
	responseFilm := &domain.ActorInfo{}
	err = json.Unmarshal(responseBytes, responseFilm)
	if err != nil {
		return nil, err
	}
	return responseFilm, nil
}

func (e externalApi) GetFilmInfo(filmURL string) (*domain.FilmInfo, error) {
	log.Println("Searching film with url", filmURL)
	responseBytes, err := utils.Get(filmURL)
	if err != nil {
		return nil, err
	}
	film := &domain.FilmInfo{}
	err = json.Unmarshal(responseBytes, film)
	if err != nil {
		return nil, err
	}
	return film, err
}

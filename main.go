package main

import (
	"fmt"
	"log"
	"project/application"
	"project/infra"
	"time"
)

func main() {
	actorID := 1
	apiData := infra.NewExternalAPI()
	app := application.NewApp(apiData)

	initialTime := time.Now()
	films, err := app.GetFilmsByActor(&actorID)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Films requested in: ", time.Since(initialTime))
	fmt.Println(films)

	initialTime = time.Now()
	films2, err := app.GetFilmsByActorWithRoutines(&actorID)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Films requested with go routines in: ", time.Since(initialTime))
	fmt.Println(films2)
}
